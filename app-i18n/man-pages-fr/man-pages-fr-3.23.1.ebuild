# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

MY_PN=${PN/-}
MY_P=${MY_PN}_${PV}
PATCH_LEVEL=1

DESCRIPTION="A collection of french Linux man pages"
HOMEPAGE="http://packages.debian.org"
SRC_URI="mirror://debian/pool/main/m/${MY_PN}/${MY_P}.orig.tar.gz
	mirror://debian/pool/main/m/${MY_PN}/${MY_P}-${PATCH_LEVEL}.diff.gz"

LICENSE="freedist"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="virtual/man"
DEPEND="sys-devel/gettext
	dev-lang/perl
	app-text/po4a"

S=${WORKDIR}/${MY_PN}-${PV}

src_unpack() {
	unpack ${A}
	mkdir "${S}"
	cd "${S}"
	mv ../{manpages,manpages-dev} .
	epatch "${WORKDIR}/${MY_P}-${PATCH_LEVEL}.diff"
}
src_install() {
	local man_pages="$(echo {manpages,manpages-dev}/fr/*/*) $(echo manpagesfr/*/*)"

	# nroff has some difficulties with charset=utf-8
	einfo "Converting man-pages to ISO-8859-1"
	for f in $man_pages; do
		iconv -c -f UTF-8 -t ISO-8859-1 $f -o $f.tmp || die "iconv failed for $f"
		mv $f.tmp $f
	done
	doman -i18n=fr $man_pages || die "doman failed"
}
