# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"

inherit gnome2

DESCRIPTION="Plugin to make XChat use the messaging indicator"
HOMEPAGE="https://launchpad.net/xchat-indicator"
SRC_URI="http://launchpad.net/${PN}/trunk/${PV}/+download/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

RDEPEND=">=x11-libs/gtk+-2.14.0
	>=dev-libs/libindicate-0.3.0
	net-irc/xchat-gnome"
DEPEND="${COMMON_DEPEND}
	dev-util/pkgconfig"

pkg_setup() {
	G2CONF="--disable-dependency-tracking --disable-static"
	DOCS="AUTHORS NEWS"
}

src_prepare() {
	gnome2_src_prepare

	# Don't use embedded header, use system header instead.
	# xchat-gnome installs its own xchat-gnome/xchat-plugin.h so we've to use it
	sed "s:\"\(xchat-plugin.h\)\":<xchat-gnome/\1>:g" -i indicator.c || die "sed failed"
}
