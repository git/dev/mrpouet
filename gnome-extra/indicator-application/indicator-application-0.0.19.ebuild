# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

GCONF_DEBUG="no"

EAPI="2"

inherit autotools eutils gnome2

DESCRIPTION="A library and indicator to take menus from applications and place them in the panel. "
HOMEPAGE="https://launchpad.net/indicator-application"
SRC_URI="http://launchpad.net/${PN}/0.1/${PV}/+download/${P}.tar.gz"

LICENSE="GPL-3 LGPL-2.1 LGPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE="doc examples mono python"

RDEPEND=">=x11-libs/gtk+-2.12
	>=dev-libs/libindicator-0.3.5
	>=dev-libs/json-glib-0.7.6
	>=dev-libs/dbus-glib-0.82
	>=dev-libs/libdbusmenu-0.2.2[gtk]
	mono? (
	dev-lang/mono
	>=dev-dotnet/gtk-sharp-2.12
	>=dev-dotnet/glib-sharp-2.12.1
	)
	python? ( >=dev-python/pygtk-2.14.0 )"
DEPEND="${RDEPEND}
	dev-util/pkgconfig
	dev-util/gtk-doc"

DOCS="ChangeLog"

pkg_setup() {
	G2CONF="--disable-dependency-tracking
		$(use_enable mono)
		$(use_enable python)"
}

src_prepare() {
	gnome2_src_prepare

	# Don't build Python and Mono bindings as default (they should be optionnal)
	epatch "${FILESDIR}"/${P}-bindings-automagic.patch

	eautoreconf
}

src_install() {
	gnome2_src_install

	if use examples; then
		docinto examples
		dodoc example/* || die "dodoc failed"
	fi
}
