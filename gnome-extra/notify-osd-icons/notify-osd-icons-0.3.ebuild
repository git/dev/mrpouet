# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit gnome2-utils

MY_P=${PN}_${PV}

DESCRIPTION="Icons for Notify-OSD in Ubuntu style"
HOMEPAGE="http://packages.ubuntu.com/"
SRC_URI="mirror://ubuntu/pool/main/n/${PN}/${MY_P}.tar.gz"

LICENSE="Attribution-ShareAlike-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

S="${WORKDIR}"/${PN}

pkg_preinst() {
	gnome2_icon_savelist
}

src_install() {
	emake DESTDIR="${D}" install || die "make install failed"
}

pkg_postinst() {
	gnome2_icon_cache_update
}
