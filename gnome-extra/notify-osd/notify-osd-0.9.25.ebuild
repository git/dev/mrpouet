# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

GCONF_DEBUG="no"

EAPI="2"

inherit eutils gnome2 autotools

DESCRIPTION="notify on-screen-display notification agent"
HOMEPAGE="https://launchpad.net/notify-osd"
SRC_URI="http://launchpad.net/${PN}/trunk/ubuntu-9.10-sru/+download/${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE="examples test"

RDEPEND=">=dev-libs/glib-2.16.0
	>=x11-libs/gtk+-2.14
	gnome-base/gconf
	x11-libs/libwnck
	>=x11-libs/libnotify-0.4.5
	>=dev-libs/dbus-glib-0.76
	!x11-misc/notification-daemon"
DEPEND="${RDEPEND}
	dev-util/pkgconfig"

#FIXME: tests/test-dbus.c segfaults on the second test case, probably because the assertion
# is executed (but I'm not sure), have a look at this crash later
# if it's not fixed on upstream
RESTRICT="test"

# ChangeLog redirects you to "bzr log" (which is ugly imho)
DOCS="AUTHORS NEWS README TODO"

pkg_setup() {
	G2CONF="$(use_with examples examples all)
		$(use_enable test tests)"
}

src_prepare() {
	gnome2_src_prepare

	epatch "${FILESDIR}"/${P}-disable-automatic-testsuite-build.patch
	eautoreconf
}

src_test() {
	emake test || die "testsuite failed"
}
