# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

NEED_PYTHON="2.5"

inherit distutils gnome2-utils

DESCRIPTION="D-Feet is a powerful graphical DBus debugger"
HOMEPAGE="http://fedorahosted.org/d-feet/"
SRC_URI="http://johnp.fedorapeople.org/downloads/${PN}/${P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="sys-apps/dbus
	>=dev-python/dbus-python-0.82.3
	dev-python/pygtk"

pkg_setup() {
	PYTHON_MODNAME="dfeet"
}
pkg_postinst() {
	distutils_pkg_postinst
	gnome2_icon_savelist
	gnome2_icon_cache_update
}
